#!/usr/bin/env bash
set -eu
awslocal s3api create-bucket --bucket acservice
cd "$(dirname "$0")/s3"
awslocal s3 sync . s3://acservice