FROM openjdk:17-alpine

COPY ./target/ac-service-*.jar /opt/service/service.jar
WORKDIR /opt/service

ENTRYPOINT ["java","-jar","service.jar"]