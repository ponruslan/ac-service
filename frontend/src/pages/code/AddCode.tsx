import React from "react";
import ListForm, {ListFormData} from "../../components/form/ListForm";
import Loader from "../../components/loader/Loader";
import {useMutation, useQuery} from "@apollo/client";
import {GET_PRODUCTS_NAMES_AND_IDS, SAVE_CODE_MUTATION} from "../../graphql/graphql-queries";
import {CodePath} from "../../constants";
import {useNavigate} from "react-router-dom";
import {Product} from "../../types";

interface ProductsData {
    products: Product[]
}

const AddCode:React.FC = () => {
    const navigate = useNavigate()
    const productsQuery = useQuery<ProductsData>(GET_PRODUCTS_NAMES_AND_IDS)
    const [mutation, {client}] = useMutation(SAVE_CODE_MUTATION)
    async function save(listFormData: ListFormData) {
        await mutation({
            variables: {
                code: {
                    name: listFormData.name,
                    content: listFormData.content,
                    productIds: listFormData.products.map(value => value.id)
                }
            }
        })
        await client.clearStore()
        navigate(CodePath)
    }
    return (
        <div className="page">
            <div className="pageTop productsTop">
                <div className="pageTitle">Добавить лист ошибок</div>
            </div>
            {productsQuery.data ?
                <ListForm withName
                          allProducts={productsQuery.data.products}
                          saveHook={save}
                />
                : <Loader/>}
        </div>
    )
}

export default AddCode