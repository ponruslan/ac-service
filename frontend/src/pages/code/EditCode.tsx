import React from "react";
import {useNavigate, useParams} from "react-router-dom";
import {useMutation, useQuery} from "@apollo/client";
import {GET_CODE, GET_PRODUCTS_NAMES_AND_IDS, SAVE_CODE_MUTATION} from "../../graphql/graphql-queries";
import ListForm, {ListFormData} from "../../components/form/ListForm";
import {CodePath} from "../../constants";
import {Code, Product} from "../../types";
import Loader from "../../components/loader/Loader";

interface CodeData {
    code: Code,
}

interface ProductsData {
    products: Product[]
}

const EditCode:React.FC = () => {
    const {id} = useParams()
    const navigate = useNavigate()
    const codeQuery = useQuery<CodeData>(GET_CODE, {variables: {id}})
    const productsQuery = useQuery<ProductsData>(GET_PRODUCTS_NAMES_AND_IDS)
    const [mutation, {client}] = useMutation(SAVE_CODE_MUTATION)
    async function save(listFormData: ListFormData) {
        await mutation({
            variables: {
                code: {
                    id: listFormData?.id,
                    name: listFormData.name,
                    content: listFormData.content,
                    productIds: listFormData.products.map(value => value.id)
                }
            }
        })
        await client.clearStore()
        navigate(CodePath)
    }
    return (
        <div className="page">
            <div className="pageTop productsTop">
                <div className="pageTitle">Редактировать лист ошибок</div>
            </div>
            {codeQuery.data && productsQuery.data ?
                <ListForm
                    withName
                    element={codeQuery.data.code}
                    allProducts={productsQuery.data.products}
                    saveHook={save}
                />
                : <Loader/>}
        </div>
    )
}

export default EditCode