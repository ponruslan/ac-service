import React from "react";
import "./navbar.scss"

export const Navbar: React.FC = () => (
    <div className="nav">
        <div className="nav-logo">
                <a href="/">AC Service</a>
        </div>
    </div>
)