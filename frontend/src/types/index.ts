export interface LoginResponse {
    login: User
}

export interface User {
    email: string,
    token: string
}

export interface Brand {
    id: number,
    name: string,
    logo: string,
    logoUrl: string
}

export interface UploadResponse {
    key: string,
    url: string
}

export interface Product {
    id: number,
    name?: string,
    type: string,
    brand: Brand,
    code?: Code,
    connect?: Connect,
    size?: Size
}

export interface Code {
    id: number,
    name: string,
    content: string,
    products: Product[]
}

export interface Connect {
    id: number,
    content: string,
    products: Product[]
}

export interface Size {
    id: number,
    content: string
    products: Product[]
}

export interface ProductFilter {
    id?: number
    brand?: BrandInput
    type?: string
}

export interface BrandInput {
    id?: number
    name?: string
}