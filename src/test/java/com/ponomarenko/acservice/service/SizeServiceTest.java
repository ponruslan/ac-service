package com.ponomarenko.acservice.service;

import com.ponomarenko.acservice.entity.Product;
import com.ponomarenko.acservice.entity.Size;
import com.ponomarenko.acservice.exception.NotFoundException;
import com.ponomarenko.acservice.repository.SizeRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SizeServiceTest {
    @Mock
    SizeRepository repository;

    @InjectMocks
    SizeService service;

    final Integer id = 1;

    @Test
    void findAll_shouldReturnSizesOrderedByNameAsc() {
        service.findAll();
        verify(repository).findAll(Sort.by(Sort.Direction.ASC, "id"));
    }

    @Test
    void findById_shouldReturnSizeIfItExists() {
        when(repository.findById(id)).thenReturn(Optional.of(new Size()));
        service.findById(id);
        verify(repository).findById(id);
    }

    @Test
    void findById_shouldThrowIfSizeNotExists() {
        when(repository.findById(id)).thenReturn(Optional.empty());
        assertThrows(NotFoundException.class, () -> service.findById(id));
    }

    @Test
    void delete_shouldDeleteSizeIfItExists() {
        when(repository.existsById(id)).thenReturn(true);
        service.delete(id);
        verify(repository).deleteById(id);
    }

    @Test
    void delete_shouldThrowIfSizeNotExists() {
        when(repository.existsById(id)).thenReturn(false);
        assertThrows(NotFoundException.class, () -> service.delete(id));
    }

    @Test
    void save_shouldCreateSizeIfIdNull() {
        var input = Size.builder()
                .content("content")
                .build();
        service.save(input);
        verify(repository).save(input);
    }

    @Test
    void save_shouldUpdateSizeIfIdNotNull() {
        var content = "content";
        var products = List.of(Product.builder().id(3).build(), Product.builder().id(4).build());
        var input = Size.builder().id(id).content(content).products(products).build();
        when(repository.existsById(id)).thenReturn(true);

        service.save(input);

        verify(repository).save(input);
    }

    @Test
    void save_shouldThrowIfSizeNotExists() {
        var input = Size.builder().id(id).build();
        when(repository.existsById(id)).thenReturn(false);
        assertThrows(NotFoundException.class, () -> service.save(input));
    }
}