package com.ponomarenko.acservice.service;

import com.ponomarenko.acservice.entity.Code;
import com.ponomarenko.acservice.entity.Product;
import com.ponomarenko.acservice.exception.NotFoundException;
import com.ponomarenko.acservice.repository.CodeRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CodeServiceTest {

    @Mock
    CodeRepository repository;

    @InjectMocks
    CodeService service;

    final Integer id = 1;

    @Test
    void findAll_shouldReturnCodesOrderedByIdAsc() {
        service.findAll();
        verify(repository).findAll(Sort.by(Sort.Direction.ASC, "name"));
    }

    @Test
    void findById_shouldReturnCodeIfItExists() {
        when(repository.findById(id)).thenReturn(Optional.of(new Code()));
        service.findById(id);
        verify(repository).findById(id);
    }

    @Test
    void findById_shouldThrowIfCodeNotExists() {
        when(repository.findById(id)).thenReturn(Optional.empty());
        assertThrows(NotFoundException.class, () -> service.findById(id));
    }

    @Test
    void delete_shouldDeleteCodeIfItExists() {
        when(repository.existsById(id)).thenReturn(true);
        service.delete(id);
        verify(repository).deleteById(id);
    }

    @Test
    void delete_shouldThrowIfCodeNotExists() {
        when(repository.existsById(id)).thenReturn(false);
        assertThrows(NotFoundException.class, () -> service.delete(id));
    }

    @Test
    void save_shouldCreateCodeIfIdNull() {
        var input = Code.builder()
                .name("name")
                .content("content")
                .build();
        service.save(input);
        verify(repository).save(input);
    }

    @Test
    void save_shouldUpdateCodeIfIdNotNull() {
        var name = "name";
        var content = "content";
        var products = List.of(Product.builder().id(3).build(), Product.builder().id(4).build());
        var input = Code.builder().id(id).name(name).content(content).products(products).build();
        when(repository.existsById(id)).thenReturn(true);

        service.save(input);

        verify(repository).save(input);
    }

    @Test
    void save_shouldThrowIfCodeNotExists() {
        var input = Code.builder().id(id).build();
        when(repository.existsById(id)).thenReturn(false);
        assertThrows(NotFoundException.class, () -> service.save(input));
    }
}