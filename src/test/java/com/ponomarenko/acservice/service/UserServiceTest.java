package com.ponomarenko.acservice.service;

import com.ponomarenko.acservice.entity.Role;
import com.ponomarenko.acservice.entity.User;
import com.ponomarenko.acservice.exception.UserAlreadyExistsException;
import com.ponomarenko.acservice.exception.UserNotFoundException;
import com.ponomarenko.acservice.repository.UserRepository;
import com.ponomarenko.acservice.security.jwt.JwtProvider;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {
    @Mock
    UserRepository userRepository;
    @Mock
    PasswordEncoder passwordEncoder;
    @Mock
    JwtProvider jwtProvider;
    @InjectMocks
    UserService userService;

    final String email = "some@email.com";
    final String password = "1234567890";
    final Integer id = 1;

    @Test
    void shouldGetUserByIdIfItExists() {
        when(userRepository.findById(id)).thenReturn(Optional.of(new User()));
        userService.findById(id);
        verify(userRepository).findById(id);
    }

    @Test
    void shouldThrowExceptionIfUserNotExists() {
        when(userRepository.findById(id)).thenReturn(Optional.empty());
        assertThrows(UserNotFoundException.class, () -> userService.findById(id));
    }

    @Test
    void shouldCheckIfUserExistsByEmail() {
        userService.existsByEmail(email);
        verify(userRepository).existsByEmail(email);
    }

    @Test
    void shouldLoadUserByEmailIfEmailExists() {
        when(userRepository.findByEmail(email)).thenReturn(Optional.of(new User()));
        userService.loadUserByUsername(email);
        verify(userRepository).findByEmail(email);
    }

    @Test
    void shouldThrowIfEmailNotExists() {
        when(userRepository.findByEmail(email)).thenReturn(Optional.empty());
        assertThrows(UsernameNotFoundException.class, () -> userService.loadUserByUsername(email));
    }

    @Test
    void shouldGenerateToken() {
        var user = new User();
        userService.generateToken(user);
        verify(jwtProvider).generateToken(user);
    }

    @Test
    void shouldCreateUserIfEmailNotExists() {
        var user = spy(User.builder().email(email).password(password).build());
        when(userRepository.existsByEmail(email)).thenReturn(false);
        userService.create(user);
        verify(passwordEncoder).encode(password);
        verify(user).setRoles(Set.of(Role.USER));
        verify(userRepository).save(any(User.class));
    }

    @Test
    void shouldThrowIfEmailExists() {
        var user = User.builder().email(email).build();
        when(userRepository.existsByEmail(email)).thenReturn(true);
        assertThrows(UserAlreadyExistsException.class, () -> userService.create(user));
    }

    @Test
    void shouldPromoteUser() {
        var roles = new HashSet<Role>();
        roles.add(Role.USER);
        var user = User.builder()
                .roles(roles)
                .build();
        when(userRepository.findById(id)).thenReturn(Optional.of(user));
        userService.promote(id);
        assertEquals(Set.of(Role.USER, Role.ADMIN), user.getRoles());
        verify(userRepository).save(user);
    }

    @Test
    void shouldUnpromoteUser() {
        var roles = new HashSet<Role>();
        roles.add(Role.USER);
        roles.add(Role.ADMIN);
        var user = User.builder()
                .roles(roles)
                .build();
        when(userRepository.findById(id)).thenReturn(Optional.of(user));
        userService.unpromote(id);
        assertEquals(Set.of(Role.USER), user.getRoles());
        verify(userRepository).save(user);
    }
}