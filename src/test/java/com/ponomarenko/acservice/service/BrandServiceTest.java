package com.ponomarenko.acservice.service;

import com.ponomarenko.acservice.entity.Brand;
import com.ponomarenko.acservice.entity.Product;
import com.ponomarenko.acservice.exception.NotFoundException;
import com.ponomarenko.acservice.repository.BrandRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BrandServiceTest {

    @Mock
    BrandRepository repository;
    @InjectMocks
    BrandService service;

    final Integer id = 1;

    @Test
    void shouldReturnBrandsOrderedByNameAsc() {
        service.findAll();
        verify(repository).findAll(Sort.by(Sort.Direction.ASC, "name"));
    }

    @Test
    void shouldReturnBrandIfItExists() {
        when(repository.findById(id)).thenReturn(Optional.of(new Brand()));
        service.findById(id);
        verify(repository).findById(id);
    }

    @Test
    void shouldThrowIfBrandNotExists() {
        when(repository.findById(id)).thenReturn(Optional.empty());
        assertThrows(NotFoundException.class, () -> service.findById(id));
    }

    @Test
    void shouldDeleteBrandIfItExists() {
        when(repository.existsById(id)).thenReturn(true);
        service.delete(id);
        verify(repository).deleteById(id);
    }

    @Test
    void delete_shouldThrowIfBrandNotExists() {
        when(repository.existsById(id)).thenReturn(false);
        assertThrows(NotFoundException.class, () -> service.delete(id));
    }

    @Test
    void shouldCreateBrandIfIdNull() {
        var brand = Brand.builder()
                .name("name")
                .logo("logo")
                .build();
        service.save(brand);
        verify(repository).save(brand);
    }

    @Test
    void shouldUpdateBrandExceptProductsFieldIfIdNotNull() {
        var name = "name";
        var logo = "logo";
        var input = Brand.builder().id(id).name(name).logo(logo).build();
        var products = List.of(Product.builder().id(1).build(), Product.builder().id(2).build());
        var toUpdate = Brand.builder().id(id).products(products).build();
        var updated = Brand.builder().id(id).name(name).logo(logo).products(products).build();
        when(repository.findById(id)).thenReturn(Optional.of(toUpdate));

        service.save(input);

        verify(repository).save(updated);
    }
}