package com.ponomarenko.acservice.service;

import com.ponomarenko.acservice.entity.Connect;
import com.ponomarenko.acservice.entity.Product;
import com.ponomarenko.acservice.exception.NotFoundException;
import com.ponomarenko.acservice.repository.ConnectRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ConnectServiceTest {
    @Mock
    ConnectRepository repository;

    @InjectMocks
    ConnectService service;

    final Integer id = 1;

    @Test
    void findAll_shouldReturnSizesOrderedByNameAsc() {
        service.findAll();
        verify(repository).findAll(Sort.by(Sort.Direction.ASC, "id"));
    }

    @Test
    void findById_shouldReturnConnectIfItExists() {
        when(repository.findById(id)).thenReturn(Optional.of(new Connect()));
        service.findById(id);
        verify(repository).findById(id);
    }

    @Test
    void findById_shouldThrowIConnectNotExists() {
        when(repository.findById(id)).thenReturn(Optional.empty());
        assertThrows(NotFoundException.class, () -> service.findById(id));
    }

    @Test
    void delete_shouldDeleteConnectIfItExists() {
        when(repository.existsById(id)).thenReturn(true);
        service.delete(id);
        verify(repository).deleteById(id);
    }

    @Test
    void delete_shouldThrowIfConnectNotExists() {
        when(repository.existsById(id)).thenReturn(false);
        assertThrows(NotFoundException.class, () -> service.delete(id));
    }

    @Test
    void save_shouldCreateConnectIfIdNull() {
        var input = Connect.builder()
                .content("content")
                .build();
        service.save(input);
        verify(repository).save(input);
    }

    @Test
    void save_shouldUpdateConnectIfIdNotNull() {
        var content = "content";
        var products = List.of(Product.builder().id(3).build(), Product.builder().id(4).build());
        var input = Connect.builder().id(id).content(content).products(products).build();
        when(repository.existsById(id)).thenReturn(true);

        service.save(input);

        verify(repository).save(input);
    }

    @Test
    void save_shouldThrowIfConnectNotExists() {
        var input = Connect.builder().id(id).build();
        when(repository.existsById(id)).thenReturn(false);
        assertThrows(NotFoundException.class, () -> service.save(input));
    }
}