package com.ponomarenko.acservice.service;

import com.ponomarenko.acservice.entity.Product;
import com.ponomarenko.acservice.exception.NotFoundException;
import com.ponomarenko.acservice.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProductServiceTest {

    @Mock
    ProductRepository repository;

    @InjectMocks
    ProductService service;

    final Integer id = 1;

    @Test
    void findAll_shouldReturnProductsOrderedByIdAsc() {
        service.findAll();
        verify(repository).findAll(Sort.by(Sort.Direction.ASC, "name"));
    }

    @Test
    void findAll_shouldReturnFilteredProductsOrderedByIdAsc() {
        var example = Example.of(Product.builder().id(1).name("name").build());
        service.findAll(example);
        verify(repository).findAll(example, Sort.by(Sort.Direction.ASC, "name"));
    }

    @Test
    void findById_shouldReturnProductIfItExists() {
        when(repository.findById(id)).thenReturn(Optional.of(new Product()));
        service.findById(id);
        verify(repository).findById(id);
    }

    @Test
    void findById_shouldThrowIfProductNotExists() {
        when(repository.findById(id)).thenReturn(Optional.empty());
        assertThrows(NotFoundException.class, () -> service.findById(id));
    }

    @Test
    void delete_shouldDeleteProductIfItExists() {
        when(repository.existsById(id)).thenReturn(true);
        service.delete(id);
        verify(repository).deleteById(id);
    }

    @Test
    void delete_shouldThrowIfProductNotExists() {
        when(repository.existsById(id)).thenReturn(false);
        assertThrows(NotFoundException.class, () -> service.delete(id));
    }

    @Test
    void save_shouldCreateProductIfIdNull() {
        var input = Product.builder()
                .name("name")
                .type("type")
                .build();
        service.save(input);
        verify(repository).save(input);
    }

    @Test
    void save_shouldUpdateProductIfIdNotNull() {
        var input = Product.builder().id(id).name("name").build();
        when(repository.existsById(id)).thenReturn(true);

        service.save(input);

        verify(repository).save(input);
    }

    @Test
    void save_shouldThrowIfProductNotExists() {
        var input = Product.builder().id(id).build();
        when(repository.existsById(id)).thenReturn(false);
        assertThrows(NotFoundException.class, () -> service.save(input));
    }
}