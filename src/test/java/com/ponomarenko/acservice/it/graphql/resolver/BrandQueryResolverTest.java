package com.ponomarenko.acservice.it.graphql.resolver;

import com.graphql.spring.boot.test.GraphQLTestTemplate;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import java.io.IOException;

import static com.ponomarenko.acservice.it.graphql.GraphQLTestHelper.GRAPHQL_QUERY_REQUEST_PATH;
import static com.ponomarenko.acservice.it.graphql.GraphQLTestHelper.GRAPHQL_QUERY_RESPONSE_PATH;
import static com.ponomarenko.acservice.it.graphql.GraphQLTestHelper.read;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.skyscreamer.jsonassert.JSONAssert.assertEquals;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class BrandQueryResolverTest {

    @Autowired
    GraphQLTestTemplate graphQLTestTemplate;

    @Test
    void brandsAreReturned() throws IOException, JSONException {
        var testName = "brands";
        var response = graphQLTestTemplate.postForResource(String.format(GRAPHQL_QUERY_REQUEST_PATH, testName));
        var expectedResponseBody = read(String.format(GRAPHQL_QUERY_RESPONSE_PATH, testName));
        assertThat(response.isOk()).isTrue();
        assertEquals(expectedResponseBody, response.getRawResponse().getBody(), true);
    }

    @Test
    void brandIsReturned() throws IOException, JSONException {
        var testName = "brand";
        var response = graphQLTestTemplate.postForResource(String.format(GRAPHQL_QUERY_REQUEST_PATH, testName));
        var expectedResponseBody = read(String.format(GRAPHQL_QUERY_RESPONSE_PATH, testName));
        assertThat(response.isOk()).isTrue();
        assertEquals(expectedResponseBody, response.getRawResponse().getBody(), true);
    }
}