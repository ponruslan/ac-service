package com.ponomarenko.acservice.it.graphql.mutation;

import com.graphql.spring.boot.test.GraphQLTestTemplate;
import com.ponomarenko.acservice.entity.Brand;
import com.ponomarenko.acservice.entity.Code;
import com.ponomarenko.acservice.entity.Connect;
import com.ponomarenko.acservice.entity.Product;
import com.ponomarenko.acservice.entity.Size;
import com.ponomarenko.acservice.service.ProductService;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.io.IOException;

import static com.ponomarenko.acservice.it.graphql.GraphQLTestHelper.ADMIN_TOKEN;
import static com.ponomarenko.acservice.it.graphql.GraphQLTestHelper.GRAPHQL_MUTATION_REQUEST_PATH;
import static com.ponomarenko.acservice.it.graphql.GraphQLTestHelper.GRAPHQL_MUTATION_RESPONSE_PATH;
import static com.ponomarenko.acservice.it.graphql.GraphQLTestHelper.read;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.when;
import static org.skyscreamer.jsonassert.JSONAssert.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductMutationTest {

    @Autowired
    GraphQLTestTemplate graphQLTestTemplate;
    @MockBean
    ProductService productService;

    @Test
    void saveProduct() throws IOException, JSONException {
        var testName = "save-product";
        var product = Product.builder()
                .id(1)
                .name("product name")
                .type("RAC")
                .brand(new Brand(1))
                .connect(new Connect(2))
                .code(new Code(3))
                .size(new Size(4))
                .build();
        when(productService.save(product)).thenReturn(product);
        var response = graphQLTestTemplate
                .withBearerAuth(ADMIN_TOKEN)
                .postForResource(String.format(GRAPHQL_MUTATION_REQUEST_PATH, testName));
        var expectedResponseBody = read(String.format(GRAPHQL_MUTATION_RESPONSE_PATH, testName));
        assertThat(response.isOk()).isTrue();
        assertEquals(expectedResponseBody, response.getRawResponse().getBody(), true);
    }

    @Test
    void saveProductWithNullValues() throws IOException, JSONException {
        var testName = "save-product-with-null-values";
        var product = Product.builder()
                .id(1)
                .name("product name")
                .type("RAC")
                .brand(new Brand(1))
                .connect(null)
                .code(null)
                .size(null)
                .build();
        when(productService.save(product)).thenReturn(product);
        var response = graphQLTestTemplate
                .withBearerAuth(ADMIN_TOKEN)
                .postForResource(String.format(GRAPHQL_MUTATION_REQUEST_PATH, testName));
        var expectedResponseBody = read(String.format(GRAPHQL_MUTATION_RESPONSE_PATH, testName));
        assertThat(response.isOk()).isTrue();
        assertEquals(expectedResponseBody, response.getRawResponse().getBody(), true);
    }

    @Test
    void deleteProduct() throws IOException, JSONException {
        var testName = "delete-product";
        when(productService.delete(1)).thenReturn(1);
        var response = graphQLTestTemplate
                .withBearerAuth(ADMIN_TOKEN)
                .postForResource(String.format(GRAPHQL_MUTATION_REQUEST_PATH, testName));
        var expectedResponseBody = read(String.format(GRAPHQL_MUTATION_RESPONSE_PATH, testName));
        assertThat(response.isOk()).isTrue();
        assertEquals(expectedResponseBody, response.getRawResponse().getBody(), true);
    }
}
