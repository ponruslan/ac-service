package com.ponomarenko.acservice.it.graphql.mutation;

import com.graphql.spring.boot.test.GraphQLTestTemplate;
import com.ponomarenko.acservice.entity.Code;
import com.ponomarenko.acservice.entity.Product;
import com.ponomarenko.acservice.service.CodeService;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.io.IOException;
import java.util.List;

import static com.ponomarenko.acservice.it.graphql.GraphQLTestHelper.ADMIN_TOKEN;
import static com.ponomarenko.acservice.it.graphql.GraphQLTestHelper.GRAPHQL_MUTATION_REQUEST_PATH;
import static com.ponomarenko.acservice.it.graphql.GraphQLTestHelper.GRAPHQL_MUTATION_RESPONSE_PATH;
import static com.ponomarenko.acservice.it.graphql.GraphQLTestHelper.read;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.when;
import static org.skyscreamer.jsonassert.JSONAssert.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CodeMutationTest {

    @Autowired
    GraphQLTestTemplate graphQLTestTemplate;
    @MockBean
    CodeService codeService;

    @Test
    void saveCode() throws IOException, JSONException {
        var testName = "save-code";
        var code = Code.builder()
                .id(1)
                .name("code name")
                .content("code content")
                .products(List.of(new Product(1), new Product(2)))
                .build();
        when(codeService.save(code)).thenReturn(code);
        var response = graphQLTestTemplate
                .withBearerAuth(ADMIN_TOKEN)
                .postForResource(String.format(GRAPHQL_MUTATION_REQUEST_PATH, testName));
        var expectedResponseBody = read(String.format(GRAPHQL_MUTATION_RESPONSE_PATH, testName));
        assertThat(response.isOk()).isTrue();
        assertEquals(expectedResponseBody, response.getRawResponse().getBody(), true);
    }

    @Test
    void deleteCode() throws IOException, JSONException {
        var testName = "delete-code";
        when(codeService.delete(1)).thenReturn(1);
        var response = graphQLTestTemplate
                .withBearerAuth(ADMIN_TOKEN)
                .postForResource(String.format(GRAPHQL_MUTATION_REQUEST_PATH, testName));
        var expectedResponseBody = read(String.format(GRAPHQL_MUTATION_RESPONSE_PATH, testName));
        assertThat(response.isOk()).isTrue();
        assertEquals(expectedResponseBody, response.getRawResponse().getBody(), true);
    }
}
