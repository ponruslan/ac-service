package com.ponomarenko.acservice.it.graphql.mutation;

import com.graphql.spring.boot.test.GraphQLTestTemplate;
import com.ponomarenko.acservice.entity.Brand;
import com.ponomarenko.acservice.service.BrandService;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.io.IOException;

import static com.ponomarenko.acservice.it.graphql.GraphQLTestHelper.ADMIN_TOKEN;
import static com.ponomarenko.acservice.it.graphql.GraphQLTestHelper.GRAPHQL_MUTATION_REQUEST_PATH;
import static com.ponomarenko.acservice.it.graphql.GraphQLTestHelper.GRAPHQL_MUTATION_RESPONSE_PATH;
import static com.ponomarenko.acservice.it.graphql.GraphQLTestHelper.read;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.when;
import static org.skyscreamer.jsonassert.JSONAssert.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BrandMutationTest {

    @Autowired
    GraphQLTestTemplate graphQLTestTemplate;
    @MockBean
    BrandService brandService;

    @Test
    void saveBrand() throws IOException, JSONException {
        var testName = "save-brand";
        var brand = Brand.builder().id(1).name("brand name").logo("logo key").build();
        when(brandService.save(brand)).thenReturn(brand);
        var response = graphQLTestTemplate
                .withBearerAuth(ADMIN_TOKEN)
                .postForResource(String.format(GRAPHQL_MUTATION_REQUEST_PATH, testName));
        var expectedResponseBody = read(String.format(GRAPHQL_MUTATION_RESPONSE_PATH, testName));
        assertThat(response.isOk()).isTrue();
        assertEquals(expectedResponseBody, response.getRawResponse().getBody(), true);
    }

    @Test
    void deleteBrand() throws IOException, JSONException {
        var testName = "delete-brand";
        when(brandService.delete(1)).thenReturn(1);
        var response = graphQLTestTemplate
                .withBearerAuth(ADMIN_TOKEN)
                .postForResource(String.format(GRAPHQL_MUTATION_REQUEST_PATH, testName));
        var expectedResponseBody = read(String.format(GRAPHQL_MUTATION_RESPONSE_PATH, testName));
        assertThat(response.isOk()).isTrue();
        assertEquals(expectedResponseBody, response.getRawResponse().getBody(), true);
    }
}
