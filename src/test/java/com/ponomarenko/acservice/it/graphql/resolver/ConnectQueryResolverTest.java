package com.ponomarenko.acservice.it.graphql.resolver;

import com.graphql.spring.boot.test.GraphQLTestTemplate;
import com.ponomarenko.acservice.it.graphql.GraphQLTestHelper;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.skyscreamer.jsonassert.JSONAssert.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ConnectQueryResolverTest {

    @Autowired
    GraphQLTestTemplate graphQLTestTemplate;

    @Test
    void connectionsAreReturned() throws IOException, JSONException {
        var testName = "connections";
        var response = graphQLTestTemplate.postForResource(String.format(GraphQLTestHelper.GRAPHQL_QUERY_REQUEST_PATH, testName));
        var expectedResponseBody = GraphQLTestHelper.read(String.format(GraphQLTestHelper.GRAPHQL_QUERY_RESPONSE_PATH, testName));
        assertThat(response.isOk()).isTrue();
        assertEquals(expectedResponseBody, response.getRawResponse().getBody(), true);
    }

    @Test
    void codeIsReturned() throws IOException, JSONException {
        var testName = "connection";
        var response = graphQLTestTemplate.postForResource(String.format(GraphQLTestHelper.GRAPHQL_QUERY_REQUEST_PATH, testName));
        var expectedResponseBody = GraphQLTestHelper.read(String.format(GraphQLTestHelper.GRAPHQL_QUERY_RESPONSE_PATH, testName));
        assertThat(response.isOk()).isTrue();
        assertEquals(expectedResponseBody, response.getRawResponse().getBody(), true);
    }
}
