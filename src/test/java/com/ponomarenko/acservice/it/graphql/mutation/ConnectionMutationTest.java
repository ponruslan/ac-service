package com.ponomarenko.acservice.it.graphql.mutation;

import com.graphql.spring.boot.test.GraphQLTestTemplate;
import com.ponomarenko.acservice.entity.Connect;
import com.ponomarenko.acservice.entity.Product;
import com.ponomarenko.acservice.service.ConnectService;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.io.IOException;
import java.util.List;

import static com.ponomarenko.acservice.it.graphql.GraphQLTestHelper.ADMIN_TOKEN;
import static com.ponomarenko.acservice.it.graphql.GraphQLTestHelper.GRAPHQL_MUTATION_REQUEST_PATH;
import static com.ponomarenko.acservice.it.graphql.GraphQLTestHelper.GRAPHQL_MUTATION_RESPONSE_PATH;
import static com.ponomarenko.acservice.it.graphql.GraphQLTestHelper.read;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.when;
import static org.skyscreamer.jsonassert.JSONAssert.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ConnectionMutationTest {

    @Autowired
    GraphQLTestTemplate graphQLTestTemplate;
    @MockBean
    ConnectService connectService;

    @Test
    void saveConnection() throws IOException, JSONException {
        var testName = "save-connection";
        var connection = Connect.builder()
                .id(1)
                .content("connection content")
                .products(List.of(new Product(1), new Product(2)))
                .build();
        when(connectService.save(connection)).thenReturn(connection);
        var response = graphQLTestTemplate
                .withBearerAuth(ADMIN_TOKEN)
                .postForResource(String.format(GRAPHQL_MUTATION_REQUEST_PATH, testName));
        var expectedResponseBody = read(String.format(GRAPHQL_MUTATION_RESPONSE_PATH, testName));
        assertThat(response.isOk()).isTrue();
        assertEquals(expectedResponseBody, response.getRawResponse().getBody(), true);
    }

    @Test
    void deleteConnection() throws IOException, JSONException {
        var testName = "delete-connection";
        when(connectService.delete(1)).thenReturn(1);
        var response = graphQLTestTemplate
                .withBearerAuth(ADMIN_TOKEN)
                .postForResource(String.format(GRAPHQL_MUTATION_REQUEST_PATH, testName));
        var expectedResponseBody = read(String.format(GRAPHQL_MUTATION_RESPONSE_PATH, testName));
        assertThat(response.isOk()).isTrue();
        assertEquals(expectedResponseBody, response.getRawResponse().getBody(), true);
    }
}
