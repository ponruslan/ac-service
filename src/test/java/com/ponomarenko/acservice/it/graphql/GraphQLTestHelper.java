package com.ponomarenko.acservice.it.graphql;

import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.nio.charset.Charset;

public abstract class GraphQLTestHelper {
    public static final String GRAPHQL_QUERY_REQUEST_PATH = "graphql/query/request/%s.graphql";
    public static final String GRAPHQL_QUERY_RESPONSE_PATH = "graphql/query/response/%s.json";
    public static final String GRAPHQL_MUTATION_REQUEST_PATH = "graphql/mutation/request/%s.graphql";
    public static final String GRAPHQL_MUTATION_RESPONSE_PATH = "graphql/mutation/response/%s.json";
    public static final String ADMIN_TOKEN = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6NDA3MDkxMjQwMCwiaWF0IjoxNjUzNTEyMDIxLCJhdXRob3JpdGllcyI6WyJVU0VSIiwiQURNSU4iXX0.XGWAe4wXTq8QN1hD2z1WCbO2OUmDbrovBxbee4SMotX4PghBLFQ95BIfhtqEvtYu3i1N2xDVNUADRd8l25erAg";


    public static String read(String location) throws IOException {
        return IOUtils.toString(new ClassPathResource(location).getInputStream(), Charset.defaultCharset());
    }
}
