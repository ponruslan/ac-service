package com.ponomarenko.acservice.it.graphql.resolver;

import com.graphql.spring.boot.test.GraphQLTestTemplate;
import com.ponomarenko.acservice.it.graphql.GraphQLTestHelper;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.skyscreamer.jsonassert.JSONAssert.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductQueryResolverTest {

    @Autowired
    GraphQLTestTemplate graphQLTestTemplate;

    @Test
    void productsAreReturned() throws IOException, JSONException {
        var testName = "products";
        var response = graphQLTestTemplate.postForResource(String.format(GraphQLTestHelper.GRAPHQL_QUERY_REQUEST_PATH, testName));
        var expectedResponseBody = GraphQLTestHelper.read(String.format(GraphQLTestHelper.GRAPHQL_QUERY_RESPONSE_PATH, testName));
        assertThat(response.isOk()).isTrue();
        assertEquals(expectedResponseBody, response.getRawResponse().getBody(), true);
    }

    @Test
    void productsWithFilterAreReturned() throws IOException, JSONException {
        var testName = "productsWithFilter";
        var response = graphQLTestTemplate.postForResource(String.format(GraphQLTestHelper.GRAPHQL_QUERY_REQUEST_PATH, testName));
        var expectedResponseBody = GraphQLTestHelper.read(String.format(GraphQLTestHelper.GRAPHQL_QUERY_RESPONSE_PATH, testName));
        assertThat(response.isOk()).isTrue();
        assertEquals(expectedResponseBody, response.getRawResponse().getBody(), true);
    }

    @Test
    void productIsReturned() throws IOException, JSONException {
        var testName = "product";
        var response = graphQLTestTemplate.postForResource(String.format(GraphQLTestHelper.GRAPHQL_QUERY_REQUEST_PATH, testName));
        var expectedResponseBody = GraphQLTestHelper.read(String.format(GraphQLTestHelper.GRAPHQL_QUERY_RESPONSE_PATH, testName));
        assertThat(response.isOk()).isTrue();
        assertEquals(expectedResponseBody, response.getRawResponse().getBody(), true);
    }
}
