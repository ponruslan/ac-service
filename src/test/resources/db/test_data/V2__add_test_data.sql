INSERT INTO public.brand (id, name, logo) VALUES (1, 'brand name 1', '1.svg');
INSERT INTO public.brand (id, name, logo) VALUES (2, 'brand name 2', '2.svg');
INSERT INTO public.brand (id, name) VALUES (3, 'brand name 3');

INSERT INTO public.code (id, content) VALUES (1, 'code content 1');
INSERT INTO public.code (id, content) VALUES (2, 'code content 2');
INSERT INTO public.code (id, content) VALUES (3, 'code content 3');

INSERT INTO public.connect (id, content) VALUES (1, 'connection content 1');
INSERT INTO public.connect (id, content) VALUES (2, 'connection content 2');
INSERT INTO public.connect (id, content) VALUES (3, 'connection content 3');

INSERT INTO public.size (id, content) VALUES (1, 'size content 1');
INSERT INTO public.size (id, content) VALUES (2, 'size content 2');
INSERT INTO public.size (id, content) VALUES (3, 'size content 3');

INSERT INTO public.product (id, name, type, brand_id, connect_id, size_id, code_id) VALUES (1, 'product name 1', 'RAC', 1, 1, 1, 1);
INSERT INTO public.product (id, name, type, brand_id, connect_id, size_id, code_id) VALUES (2, 'product name 2', 'LCAC', 2, 2, 2, 2);
INSERT INTO public.product (id, name, type, brand_id, connect_id, size_id, code_id) VALUES (3, 'product name 3', 'RAC', 3, 3, 3, 3);


INSERT INTO users (id, email, password) VALUES (1, 'admin', '$2a$08$1gfuP1B.jEET634UVFYereoldTP31F3iS06vf8BEqxBHeWk4EEfJy');

INSERT INTO user_role (user_id, roles) VALUES (1, 'USER');
INSERT INTO user_role (user_id, roles) VALUES (1, 'ADMIN');