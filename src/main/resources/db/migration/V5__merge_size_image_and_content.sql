UPDATE size
SET content = CONCAT('<img src="/image/', image, '">', content)
where image IS NOT NULL;

alter table size drop column image;