INSERT INTO users (id, email, password) VALUES (1, 'admin', '$2a$06$.z45RNxWeviJyVZhjMCtl.FSEGQdpbK7FD5pVlL/ADI0Nli3iSO4.');

ALTER SEQUENCE users_id_seq RESTART WITH 2;

INSERT INTO user_role (user_id, roles) VALUES (1, 'USER');
INSERT INTO user_role (user_id, roles) VALUES (1, 'ADMIN');