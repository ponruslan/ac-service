package com.ponomarenko.acservice.rest;

import com.ponomarenko.acservice.s3.S3Service;
import com.ponomarenko.acservice.s3.properties.AwsProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import software.amazon.awssdk.services.s3.model.NoSuchBucketException;
import software.amazon.awssdk.services.s3.model.NoSuchKeyException;

import java.io.IOException;

import static com.ponomarenko.acservice.Constants.IMAGE_PATH;

@Controller
@RequiredArgsConstructor
@Slf4j
@RequestMapping(IMAGE_PATH)
public class ImageController {

    private final S3Service s3Service;
    private final AwsProperties properties;

    @GetMapping(value = "/{uuid}")
    @Cacheable("uuid")
    public ResponseEntity<byte[]> getImage(@PathVariable String uuid) throws IOException {
        var responseInputStream = s3Service.getObject(uuid, properties.getS3().getBucketName());
        var mediaType = MediaType.parseMediaType(responseInputStream.response().contentType());
        var contentLength = responseInputStream.response().contentLength();
        return ResponseEntity.ok()
                .contentType(mediaType)
                .contentLength(contentLength)
                .body(responseInputStream.readAllBytes());
    }

    @ExceptionHandler(NoSuchKeyException.class)
    public ResponseEntity<?> handleKeyNotFoundException() {
        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler(NoSuchBucketException.class)
    public ResponseEntity<?> handleBucketNotFoundException() {
        log.error("Can't receive image, bucket '{}' is not exists", properties.getS3().getBucketName());
        return ResponseEntity.notFound().build();
    }
}
