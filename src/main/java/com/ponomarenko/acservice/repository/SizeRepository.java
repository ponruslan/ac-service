package com.ponomarenko.acservice.repository;

import com.ponomarenko.acservice.entity.Size;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SizeRepository extends JpaRepository<Size, Integer> {
}
