package com.ponomarenko.acservice.repository;

import com.ponomarenko.acservice.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Integer> {
}
