package com.ponomarenko.acservice.repository;

import com.ponomarenko.acservice.entity.Code;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CodeRepository extends JpaRepository<Code, Integer> {
}
