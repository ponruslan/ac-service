package com.ponomarenko.acservice.repository;

import com.ponomarenko.acservice.entity.Connect;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConnectRepository extends JpaRepository<Connect, Integer> {
}
