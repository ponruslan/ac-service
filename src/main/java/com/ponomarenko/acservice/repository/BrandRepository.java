package com.ponomarenko.acservice.repository;

import com.ponomarenko.acservice.entity.Brand;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BrandRepository extends JpaRepository<Brand, Integer> {
}
