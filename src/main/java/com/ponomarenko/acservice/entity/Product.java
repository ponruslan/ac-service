package com.ponomarenko.acservice.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String type;
    @ToString.Exclude
    @ManyToOne(fetch = FetchType.EAGER)
    private Brand brand;
    @ToString.Exclude
    @ManyToOne(fetch = FetchType.EAGER)
    private Connect connect;
    @ToString.Exclude
    @ManyToOne(fetch = FetchType.EAGER)
    private Code code;
    @ToString.Exclude
    @ManyToOne(fetch = FetchType.EAGER)
    private Size size;

    public Product(Integer id) {
        this.id = id;
    }
}
