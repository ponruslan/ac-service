package com.ponomarenko.acservice.s3.properties;

import lombok.Data;
import software.amazon.awssdk.regions.Region;

import javax.validation.constraints.NotNull;

@Data
public class S3Properties {

    @NotNull
    private String bucketName;
    private boolean autoCreateBucket;
    @NotNull
    private Region region;
    private String endpointOverride;
}
