package com.ponomarenko.acservice.s3.properties;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CredentialsProperties {
    @NotNull
    private String accessKey;
    @NotNull
    private String secretKey;
}
