package com.ponomarenko.acservice.s3.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

@Data
@Validated
@ConfigurationProperties(prefix = "aws")
public class AwsProperties {
    private CredentialsProperties credentials;
    private S3Properties s3;
}
