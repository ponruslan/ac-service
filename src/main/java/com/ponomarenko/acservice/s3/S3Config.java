package com.ponomarenko.acservice.s3;

import com.ponomarenko.acservice.s3.properties.AwsProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.services.s3.S3Client;

import java.net.URI;

@Configuration
@RequiredArgsConstructor
public class S3Config {

    private final AwsProperties properties;

    @Bean
    public S3Client s3Client(AwsCredentialsProvider credentialsProvider) {
        var s3ClientBuilder = S3Client.builder()
                .region(properties.getS3().getRegion())
                .credentialsProvider(credentialsProvider);
        if (properties.getS3().getEndpointOverride() != null) {
            s3ClientBuilder.endpointOverride(URI.create(properties.getS3().getEndpointOverride()));
        }
        return s3ClientBuilder.build();
    }

    @Bean
    public AwsCredentialsProvider getProvider() {
        return StaticCredentialsProvider.create(
                AwsBasicCredentials.create(
                        properties.getCredentials().getAccessKey(),
                        properties.getCredentials().getSecretKey()
                ));
    }
}
