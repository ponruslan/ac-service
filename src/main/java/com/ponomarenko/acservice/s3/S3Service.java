package com.ponomarenko.acservice.s3;

import com.ponomarenko.acservice.s3.exception.BucketNotExistsException;
import com.ponomarenko.acservice.s3.properties.AwsProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.tika.mime.MimeTypeException;
import org.apache.tika.mime.MimeTypes;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.core.ResponseInputStream;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

import javax.servlet.http.Part;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.UUID;


@Slf4j
@Service
@RequiredArgsConstructor
public class S3Service {

    private final AwsProperties properties;
    private final S3Client s3Client;

    public ResponseInputStream<GetObjectResponse> getObject(String uuid, String bucketName) {
        var request = GetObjectRequest.builder()
                .bucket(bucketName)
                .key(uuid)
                .build();
        return s3Client.getObject(request);
    }

    public String uploadFile(Part part) throws IOException {
        var inputStream = part.getInputStream();
        var contentType = part.getContentType();
        var contentLength = part.getSize();
        var key = generateKeyFromContentType(contentType);
        return upload(properties.getS3().getBucketName(), key, inputStream, contentType, contentLength);
    }

    public String uploadFile(File file, String bucketName) {
        try {
            var inputStream = new FileInputStream(file);
            var contentType = Files.probeContentType(file.toPath());
            var contentLength = Files.size(file.toPath());
            var key = generateKeyFromContentType(contentType);
            return upload(bucketName, key, inputStream, contentType, contentLength);
        } catch (IOException e) {
            log.error("File read exception target file '{}'", file.getName());
            return null;
        }
    }

    private String generateKeyFromContentType(String contentType) {
        MimeTypes allTypes = MimeTypes.getDefaultMimeTypes();
        var key = UUID.randomUUID().toString();
        try {
            var extension = allTypes.forName(contentType).getExtension();
            return key.concat(extension);
        } catch (MimeTypeException e) {
            return key;
        }
    }

    @Deprecated
    private String generateKey(String originalFileName) {
        var key = UUID.randomUUID().toString();
        var extension = FilenameUtils.getExtension(originalFileName);
        return key.concat(".").concat(extension.toLowerCase());
    }

    private String upload(String bucketName,
                          String key,
                          InputStream inputStream,
                          String contentType,
                          Long contentLength) {
        var bucketNotExists = !bucketExists(bucketName);
        if (bucketNotExists && properties.getS3().isAutoCreateBucket()) {
            createBucket(bucketName);
        } else if (bucketNotExists) {
            throw new BucketNotExistsException(bucketName);
        }
        var objectRequest = PutObjectRequest.builder()
                .bucket(bucketName)
                .key(key)
                .contentType(contentType)
                .contentLength(contentLength)
                .metadata(new HashMap<>())
                .build();
        s3Client.putObject(objectRequest, RequestBody.fromInputStream(inputStream, contentLength));
        log.info("File with key '{}' uploaded to '{}' bucket", key, bucketName);
        return key;
    }

    private boolean bucketExists(String bucketName) {
        return s3Client.listBuckets()
                .buckets()
                .stream()
                .anyMatch(bucket -> bucket.name().equals(bucketName));
    }

    private void createBucket(String bucketName) {
        s3Client.createBucket(builder -> builder.bucket(bucketName));
        log.info("Bucket with name '{}' created", bucketName);
    }
}
