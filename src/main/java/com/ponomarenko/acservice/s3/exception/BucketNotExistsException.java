package com.ponomarenko.acservice.s3.exception;

import lombok.RequiredArgsConstructor;

import java.text.MessageFormat;

@RequiredArgsConstructor
public class BucketNotExistsException extends RuntimeException {

    private final String bucketName;

    @Override
    public String getMessage() {
        return MessageFormat.format("Bucket with name ''{0}'' doesn't exists", bucketName);
    }
}
