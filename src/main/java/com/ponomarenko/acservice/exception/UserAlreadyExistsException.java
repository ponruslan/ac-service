package com.ponomarenko.acservice.exception;

public class UserAlreadyExistsException extends RuntimeException {

    @Override
    public String getMessage() {
        return "User with this email already exists";
    }
}
