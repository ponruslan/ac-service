package com.ponomarenko.acservice.exception;

public class NotFoundException extends RuntimeException {

    @Override
    public String getMessage() {
        return "Item Not found";
    }
}
