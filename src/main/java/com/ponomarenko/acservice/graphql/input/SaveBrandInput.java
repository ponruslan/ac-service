package com.ponomarenko.acservice.graphql.input;

import lombok.Data;

@Data
public class SaveBrandInput {
    private Integer id;
    private String name;
    private String logo;
}
