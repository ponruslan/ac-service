package com.ponomarenko.acservice.graphql.input;

import lombok.Data;

import java.util.Set;

@Data
public class SaveCodeInput {
    private Integer id;
    private String name;
    private String content;
    private Set<Integer> productIds;
}
