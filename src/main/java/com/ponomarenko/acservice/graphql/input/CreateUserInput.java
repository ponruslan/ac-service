package com.ponomarenko.acservice.graphql.input;

import lombok.Data;

@Data
public class CreateUserInput {
    private final String email;
    private final String password;
}
