package com.ponomarenko.acservice.graphql.input;

import lombok.Data;

import java.util.Set;

@Data
public class SaveSizeInput {
    private Integer id;
    private String content;
    private Set<Integer> productIds;
}
