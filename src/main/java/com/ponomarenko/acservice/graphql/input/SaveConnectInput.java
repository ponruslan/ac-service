package com.ponomarenko.acservice.graphql.input;

import lombok.Data;

import java.util.Set;

@Data
public class SaveConnectInput {
    private Integer id;
    private String content;
    private Set<Integer> productIds;
}
