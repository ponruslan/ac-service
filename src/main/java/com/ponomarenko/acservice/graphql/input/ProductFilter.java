package com.ponomarenko.acservice.graphql.input;

import com.ponomarenko.acservice.entity.Product;
import com.ponomarenko.acservice.graphql.input.BrandInput;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.domain.Example;

@Data
@Builder
public class ProductFilter {
    private Integer id;
    private BrandInput brand;
    private String type;

    public Example<Product> getExample() {
        Product product = Product.builder()
                .id(this.id)
                .type(this.type)
                .build();
        if (brand != null) {
            product.setBrand(this.brand.toBrand());
        }
        return Example.of(product);
    }
}
