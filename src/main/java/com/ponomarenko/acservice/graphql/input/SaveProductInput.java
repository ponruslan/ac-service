package com.ponomarenko.acservice.graphql.input;

import lombok.Data;

@Data
public class SaveProductInput {
    private Integer id;
    private String name;
    private String type;
    private Integer brandId;
    private Integer connectionId;
    private Integer codeId;
    private Integer sizeId;
}
