package com.ponomarenko.acservice.graphql.input;

import com.ponomarenko.acservice.entity.Brand;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BrandInput {
    private Integer id;
    private String name;

    public Brand toBrand() {
        return Brand.builder()
                .id(id)
                .name(name)
                .build();
    }
}
