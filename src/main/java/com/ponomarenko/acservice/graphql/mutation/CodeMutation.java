package com.ponomarenko.acservice.graphql.mutation;

import com.ponomarenko.acservice.entity.Code;
import com.ponomarenko.acservice.entity.Product;
import com.ponomarenko.acservice.graphql.input.SaveCodeInput;
import com.ponomarenko.acservice.service.CodeService;
import graphql.kickstart.tools.GraphQLMutationResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@PreAuthorize("hasAuthority('ADMIN')")
public class CodeMutation implements GraphQLMutationResolver {
    private final CodeService codeService;

    public Code saveCode(SaveCodeInput input) {
        var products = input.getProductIds().stream().map(Product::new).toList();
        var code = Code.builder()
                .id(input.getId())
                .name(input.getName())
                .content(input.getContent())
                .products(products)
                .build();
        return codeService.save(code);
    }

    public Integer deleteCode(Integer id) {
        return codeService.delete(id);
    }
}
