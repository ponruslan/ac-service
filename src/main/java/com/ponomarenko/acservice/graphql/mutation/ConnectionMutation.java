package com.ponomarenko.acservice.graphql.mutation;

import com.ponomarenko.acservice.entity.Connect;
import com.ponomarenko.acservice.entity.Product;
import com.ponomarenko.acservice.graphql.input.SaveConnectInput;
import com.ponomarenko.acservice.service.ConnectService;
import graphql.kickstart.tools.GraphQLMutationResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@PreAuthorize("hasAuthority('ADMIN')")
public class ConnectionMutation implements GraphQLMutationResolver {
    private final ConnectService connectService;

    public Connect saveConnection(SaveConnectInput input) {
        var products = input.getProductIds().stream().map(Product::new).toList();
        var connect = Connect.builder()
                .id(input.getId())
                .content(input.getContent())
                .products(products)
                .build();
        return connectService.save(connect);
    }

    public Integer deleteConnection(Integer id) {
        return connectService.delete(id);
    }
}
