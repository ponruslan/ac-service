package com.ponomarenko.acservice.graphql.mutation;

import com.ponomarenko.acservice.entity.User;
import com.ponomarenko.acservice.graphql.input.CreateUserInput;
import com.ponomarenko.acservice.service.UserService;
import graphql.kickstart.tools.GraphQLMutationResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UserMutation implements GraphQLMutationResolver {

    private final UserService userService;
    private final AuthenticationProvider authenticationProvider;

    public User login(String email, String password) {
        UsernamePasswordAuthenticationToken credentials = new UsernamePasswordAuthenticationToken(email, password);
        try {
            SecurityContextHolder.getContext().setAuthentication(authenticationProvider.authenticate(credentials));
            return userService.getCurrentUser();
        } catch (AuthenticationException ex) {
            throw new BadCredentialsException(email);
        }
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    public User createUser(CreateUserInput input) {
        var user = User.builder()
                .email(input.getEmail())
                .password(input.getPassword())
                .build();
        return userService.create(user);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    public User promoteUser(Integer id) {
        return userService.promote(id);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    public User unpromoteUser(Integer id) {
        return userService.unpromote(id);
    }
}
