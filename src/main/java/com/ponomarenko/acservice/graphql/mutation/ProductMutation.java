package com.ponomarenko.acservice.graphql.mutation;

import com.ponomarenko.acservice.entity.Brand;
import com.ponomarenko.acservice.entity.Code;
import com.ponomarenko.acservice.entity.Connect;
import com.ponomarenko.acservice.entity.Product;
import com.ponomarenko.acservice.entity.Size;
import com.ponomarenko.acservice.graphql.input.SaveProductInput;
import com.ponomarenko.acservice.service.ProductService;
import graphql.kickstart.tools.GraphQLMutationResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@RequiredArgsConstructor
@PreAuthorize("hasAuthority('ADMIN')")
public class ProductMutation implements GraphQLMutationResolver {
    private final ProductService productService;

    public Product saveProduct(SaveProductInput input) {
        var brand = new Brand(Objects.requireNonNull(input.getBrandId()));
        var code = input.getCodeId() == null ? null : new Code(input.getCodeId());
        var size = input.getSizeId() == null ? null : new Size(input.getSizeId());
        var connect = input.getConnectionId() == null ? null : new Connect(input.getConnectionId());
        var product = Product.builder()
                .id(input.getId())
                .name(input.getName())
                .type(input.getType())
                .brand(brand)
                .connect(connect)
                .code(code)
                .size(size)
                .build();
        return productService.save(product);
    }

    public Integer deleteProduct(Integer id) {
        return productService.delete(id);
    }
}
