package com.ponomarenko.acservice.graphql.mutation;

import com.ponomarenko.acservice.s3.S3Service;
import graphql.kickstart.tools.GraphQLMutationResolver;
import graphql.schema.DataFetchingEnvironment;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import javax.servlet.http.Part;
import java.io.IOException;
import java.util.List;

import static com.ponomarenko.acservice.Constants.IMAGE_PATH;

@Component
@Slf4j
@RequiredArgsConstructor
@PreAuthorize("hasAuthority('ADMIN')")
public class UploadFileMutation implements GraphQLMutationResolver {

    private final S3Service s3Service;

    public UploadResponse uploadImage(List<Part> files, DataFetchingEnvironment environment) throws IOException {
        List<Part> filesList = environment.getArgument("files");
        Part actualFile = filesList.get(0);

        var key = s3Service.uploadFile(actualFile);
        var url = IMAGE_PATH.concat(key);
        return UploadResponse.builder()
                .key(key)
                .url(url)
                .build();
    }

    @Data
    @Builder
    public static class UploadResponse {
        public String key;
        public String url;
    }
}
