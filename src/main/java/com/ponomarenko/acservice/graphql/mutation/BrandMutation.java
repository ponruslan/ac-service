package com.ponomarenko.acservice.graphql.mutation;

import com.ponomarenko.acservice.entity.Brand;
import com.ponomarenko.acservice.graphql.input.SaveBrandInput;
import com.ponomarenko.acservice.service.BrandService;
import graphql.kickstart.tools.GraphQLMutationResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@PreAuthorize("hasAuthority('ADMIN')")
public class BrandMutation implements GraphQLMutationResolver {
    private final BrandService service;

    public Brand saveBrand(SaveBrandInput input) {
        var brand = Brand.builder()
                .id(input.getId())
                .name(input.getName())
                .logo(input.getLogo())
                .build();
        return service.save(brand);
    }

    public Integer deleteBrand(Integer id) {
        return service.delete(id);
    }

}
