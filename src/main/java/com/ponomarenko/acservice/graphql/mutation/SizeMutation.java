package com.ponomarenko.acservice.graphql.mutation;

import com.ponomarenko.acservice.entity.Product;
import com.ponomarenko.acservice.entity.Size;
import com.ponomarenko.acservice.graphql.input.SaveSizeInput;
import com.ponomarenko.acservice.service.SizeService;
import graphql.kickstart.tools.GraphQLMutationResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@PreAuthorize("hasAuthority('ADMIN')")
public class SizeMutation implements GraphQLMutationResolver {
    private final SizeService sizeService;

    public Size saveSize(SaveSizeInput input) {
        var products = input.getProductIds().stream().map(Product::new).toList();
        var size = Size.builder()
                .id(input.getId())
                .content(input.getContent())
                .products(products)
                .build();
        return sizeService.save(size);
    }

    public Integer deleteSize(Integer id) {
        return sizeService.delete(id);
    }
}
