package com.ponomarenko.acservice.graphql.resolver;

import com.ponomarenko.acservice.entity.Product;
import com.ponomarenko.acservice.graphql.input.ProductFilter;
import com.ponomarenko.acservice.service.ProductService;
import graphql.kickstart.tools.GraphQLQueryResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class ProductQueryResolver implements GraphQLQueryResolver {
    private final ProductService productService;

    public List<Product> products(ProductFilter where) {
        if (where == null) {
            return productService.findAll();
        }
        return productService.findAll(where.getExample());
    }

    public Product product(Integer id) {
        return productService.findById(id);
    }
}
