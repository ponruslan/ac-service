package com.ponomarenko.acservice.graphql.resolver;

import com.ponomarenko.acservice.entity.Brand;
import graphql.kickstart.tools.GraphQLResolver;
import org.springframework.stereotype.Component;

import static com.ponomarenko.acservice.Constants.IMAGE_PATH;

@Component
public class BrandResolver implements GraphQLResolver<Brand> {

    public String getLogoUrl(Brand brand) {
        if (brand.getLogo() == null) {
            return null;
        }
        return IMAGE_PATH.concat(brand.getLogo());
    }
}
