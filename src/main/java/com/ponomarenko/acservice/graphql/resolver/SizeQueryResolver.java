package com.ponomarenko.acservice.graphql.resolver;

import com.ponomarenko.acservice.entity.Size;
import com.ponomarenko.acservice.service.SizeService;
import graphql.kickstart.tools.GraphQLQueryResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class SizeQueryResolver implements GraphQLQueryResolver {

    private final SizeService service;

    public List<Size> sizes() {
        return service.findAll();
    }

    public Size size(Integer id) {
        return service.findById(id);
    }
}
