package com.ponomarenko.acservice.graphql.resolver;

import com.ponomarenko.acservice.entity.Brand;
import com.ponomarenko.acservice.service.BrandService;
import graphql.kickstart.tools.GraphQLQueryResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class BrandQueryResolver implements GraphQLQueryResolver {
    private final BrandService service;

    public List<Brand> brands() {
        return service.findAll();
    }

    public Brand brand(Integer id) {
        return service.findById(id);
    }
}
