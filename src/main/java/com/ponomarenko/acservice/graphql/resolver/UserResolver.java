package com.ponomarenko.acservice.graphql.resolver;

import com.ponomarenko.acservice.entity.User;
import com.ponomarenko.acservice.service.UserService;
import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UserResolver implements GraphQLResolver<User> {

    private final UserService userService;

    @PreAuthorize("isAuthenticated()")
    public String getToken(User user) {
        return userService.generateToken(user);
    }
}
