package com.ponomarenko.acservice.graphql.resolver;

import com.ponomarenko.acservice.service.CodeService;
import com.ponomarenko.acservice.entity.Code;
import graphql.kickstart.tools.GraphQLQueryResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class CodeQueryResolver implements GraphQLQueryResolver {

    private final CodeService service;

    public List<Code> codes() {
        return service.findAll();
    }

    public Code code(Integer id) {
        return service.findById(id);
    }

}
