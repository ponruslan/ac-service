package com.ponomarenko.acservice.graphql.resolver;

import com.ponomarenko.acservice.entity.User;
import com.ponomarenko.acservice.service.UserService;
import graphql.kickstart.tools.GraphQLQueryResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UserQueryResolver implements GraphQLQueryResolver {

    private final UserService userService;

    @PreAuthorize("isAuthenticated()")
    public User me() {
        return userService.getCurrentUser();
    }
}
