package com.ponomarenko.acservice.graphql.resolver;

import com.ponomarenko.acservice.service.ConnectService;
import com.ponomarenko.acservice.entity.Connect;
import graphql.kickstart.tools.GraphQLQueryResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class ConnectQueryResolver implements GraphQLQueryResolver {
    private final ConnectService service;

    public List<Connect> connections() {
        return service.findAll();
    }

    public Connect connection(Integer id) {
        return service.findById(id);
    }
}
