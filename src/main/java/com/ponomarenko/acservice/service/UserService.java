package com.ponomarenko.acservice.service;

import com.ponomarenko.acservice.entity.Role;
import com.ponomarenko.acservice.entity.User;
import com.ponomarenko.acservice.exception.UserAlreadyExistsException;
import com.ponomarenko.acservice.exception.UserNotFoundException;
import com.ponomarenko.acservice.repository.UserRepository;
import com.ponomarenko.acservice.security.jwt.JwtProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtProvider jwtProvider;

    public User findById(Integer id) {
        return userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
    }

    public boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByEmail(username)
                .orElseThrow(() -> new UsernameNotFoundException("Invalid username or password"));
    }

    public User getCurrentUser() {
        return Optional
                .ofNullable(SecurityContextHolder.getContext())
                .map(SecurityContext::getAuthentication)
                .map(Authentication::getName)
                .flatMap(userRepository::findByEmail)
                .orElse(null);
    }

    public User create(User user) {
        if (!existsByEmail(user.getEmail())) {
            user.setRoles(Set.of(Role.USER));
            var encodedPassword = passwordEncoder.encode(user.getPassword());
            user.setPassword(encodedPassword);
            return userRepository.save(user);
        }
        throw new UserAlreadyExistsException();
    }

    public User promote(Integer userId) {
        var user = findById(userId);
        user.getRoles().add(Role.ADMIN);
        return userRepository.save(user);
    }

    public User unpromote(Integer id) {
        var user = findById(id);
        user.getRoles().remove(Role.ADMIN);
        return userRepository.save(user);
    }

    public String generateToken(User user) {
        return jwtProvider.generateToken(user);
    }
}
