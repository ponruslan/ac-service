package com.ponomarenko.acservice.service;

import com.ponomarenko.acservice.entity.Product;
import com.ponomarenko.acservice.exception.NotFoundException;
import com.ponomarenko.acservice.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;

    public List<Product> findAll() {
        return productRepository.findAll(getDefaultSorting());
    }

    public List<Product> findAll(Example<Product> example) {
        return productRepository.findAll(example, getDefaultSorting());
    }

    public Product findById(Integer id) {
        return productRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    private Sort getDefaultSorting() {
        return Sort.by(Sort.Direction.ASC, "name");
    }

    public Product save(Product input) {
        return input.getId() == null ? create(input) : update(input);
    }

    private Product create(Product input) {
        return productRepository.save(input);
    }

    private Product update(Product input) {
        if (productRepository.existsById(input.getId())) {
            return productRepository.save(input);
        }
        throw new NotFoundException();
    }

    public Integer delete(Integer id) {
        if (productRepository.existsById(id)) {
            productRepository.deleteById(id);
            return id;
        }
        throw new NotFoundException();
    }
}
