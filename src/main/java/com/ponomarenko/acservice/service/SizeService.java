package com.ponomarenko.acservice.service;

import com.ponomarenko.acservice.entity.Size;
import com.ponomarenko.acservice.exception.NotFoundException;
import com.ponomarenko.acservice.repository.SizeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SizeService {
    private final SizeRepository sizeRepository;

    public List<Size> findAll() {
        return sizeRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
    }

    public Size findById(Integer id) {
        return sizeRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    public Size save(Size input) {
        return input.getId() == null ? create(input) : update(input);
    }

    private Size update(Size input) {
        if (sizeRepository.existsById(input.getId())) {
            return sizeRepository.save(input);
        }
        throw new NotFoundException();
    }

    private Size create(Size input) {
        return sizeRepository.save(input);
    }

    public Integer delete(Integer id) {
        if (sizeRepository.existsById(id)) {
            sizeRepository.deleteById(id);
            return id;
        }
        throw new NotFoundException();
    }
}
