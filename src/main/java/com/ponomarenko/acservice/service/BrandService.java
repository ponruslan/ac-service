package com.ponomarenko.acservice.service;

import com.ponomarenko.acservice.entity.Brand;
import com.ponomarenko.acservice.exception.NotFoundException;
import com.ponomarenko.acservice.repository.BrandRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BrandService {
    private final BrandRepository repository;

    public List<Brand> findAll() {
        return repository.findAll(Sort.by(Sort.Direction.ASC, "name"));
    }

    public Brand findById(Integer id) {
        return repository.findById(id).orElseThrow(NotFoundException::new);
    }

    public Brand save(Brand brand) {
        return brand.getId() == null ? create(brand) : update(brand);
    }

    public Integer delete(Integer id) {
        if (repository.existsById(id)) {
            repository.deleteById(id);
            return id;
        }
        throw new NotFoundException();
    }

    private Brand create(Brand brand) {
        return repository.save(brand);
    }

    private Brand update(Brand brand) {
        var brandEntity = repository.findById(brand.getId())
                .orElseThrow(NotFoundException::new);
        brandEntity.setName(brand.getName());
        brandEntity.setLogo(brand.getLogo());
        return repository.save(brandEntity);
    }
}
