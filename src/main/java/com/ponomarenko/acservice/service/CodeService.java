package com.ponomarenko.acservice.service;

import com.ponomarenko.acservice.entity.Code;
import com.ponomarenko.acservice.exception.NotFoundException;
import com.ponomarenko.acservice.repository.CodeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CodeService {
    private final CodeRepository codeRepository;

    public List<Code> findAll() {
        return codeRepository.findAll(Sort.by(Sort.Direction.ASC, "name"));
    }

    public Code findById(Integer id) {
        return codeRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    public Code save(Code input) {
        return input.getId() == null ? create(input) : update(input);
    }

    private Code update(Code input) {
        if (codeRepository.existsById(input.getId())) {
            return codeRepository.save(input);
        }
        throw new NotFoundException();
    }

    private Code create(Code input) {
        return codeRepository.save(input);
    }

    public Integer delete(Integer id) {
        if (codeRepository.existsById(id)) {
            codeRepository.deleteById(id);
            return id;
        }
        throw new NotFoundException();
    }
}
