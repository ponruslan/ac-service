package com.ponomarenko.acservice.service;

import com.ponomarenko.acservice.entity.Connect;
import com.ponomarenko.acservice.exception.NotFoundException;
import com.ponomarenko.acservice.repository.ConnectRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class ConnectService {
    private final ConnectRepository connectRepository;

    public List<Connect> findAll() {
        return connectRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
    }

    public Connect findById(Integer id) {
        return connectRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    public Connect save(Connect input) {
        return input.getId() == null ? create(input) : update(input);
    }

    private Connect update(Connect input) {
        if (connectRepository.existsById(input.getId())) {
            return connectRepository.save(input);
        }
        throw new NotFoundException();
    }

    private Connect create(Connect input) {
        return connectRepository.save(input);
    }

    public Integer delete(Integer id) {
        if (connectRepository.existsById(id)) {
            connectRepository.deleteById(id);
            return id;
        }
        throw new NotFoundException();
    }
}
