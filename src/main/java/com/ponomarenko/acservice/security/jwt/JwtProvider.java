package com.ponomarenko.acservice.security.jwt;

import com.ponomarenko.acservice.entity.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;

@Component
public class JwtProvider {

    private final SignatureAlgorithm algorithm = SignatureAlgorithm.HS512;
    @Value("${jwt.secret}")
    private String secret;
    @Value("${jwt.expiration}")
    private String expirationTime;


    public String generateToken(User user) {
        var claims = new HashMap<String, Object>();
        claims.put("authorities", user.getAuthorities());
        var expirationSeconds = Long.parseLong(expirationTime);
        var creationDate = new Date(System.currentTimeMillis());
        var expirationDate = new Date(creationDate.getTime() + expirationSeconds * 1000);
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(user.getUsername())
                .setIssuedAt(creationDate)
                .setExpiration(expirationDate)
                .signWith(algorithm, secret)
                .compact();
    }

    public String extractUsername(String token) {
        Claims claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
        return claims.getSubject();
    }

    public void validateToken(String token) {
        Jwts.parser().setSigningKey(secret).parseClaimsJws(token);
    }
}
