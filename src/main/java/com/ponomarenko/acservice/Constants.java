package com.ponomarenko.acservice;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constants {
    public final static String IMAGE_PATH = "/image/";
}
