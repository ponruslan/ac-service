# Welcome to Air Cooler Service

This service provides a manual for the installation and fixing of air coolers for service workers.

## Local environment setup

- Java 17
- Docker
- LocalStack AWS CLI
- Node 16
- Yarn

## How to run locally
PostgreSQL and S3 `docker-compose -f ./local/docker-compose.yml up -d`
### To run frontend with backend part
```shell
mvn spring-boot:run
```

### To run frontend separately from backend
```shell
# run backand part
mvn spring-boot:run
# run frontend part
cd frontend
yarn install
yarn start
```
If you have [LocalStack AWS CLI](https://github.com/localstack/awscli-local) installed, you can sync files from `/local/s3` with localstack, 
just run `./local/add-files-to-s3.sh`

Once the service is running, you can hit its health check: `curl http://localhost:8080/actuator/health`

### To run service with prod resources

```shell
# Copy env file with secrets from s3
aws cp s3://acservice-secrets/production.env .
# Run service with prod variables
export $(cat production.env | xargs) && mvn spring-boot:run
```
